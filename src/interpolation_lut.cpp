#include <Rcpp.h>
#include <strings.h>
using namespace Rcpp;

/*
 * Function to get the closest index of the target into the LUT.
 */
void get_indice(std::vector<double> vec, double target, int &ii, double &rr) {

  // std::cout << target << "****" << std::endl;

for(int i = 0; i < vec.size(); i += 1) {

  if(target >= vec[i] && target < vec[i + 1]) {
    ii = i;
  }
}

// std::cout << target << ":" <<  vec[0] << std::endl;

if(target < vec[0]) {
  ii = 1;
  rr = 0;
} else {
  rr = (target - vec[ii]) / (vec[ii + 1] - vec[ii]);
}

}

void to_5d(NumericVector v, double ptr[83][19][10][8][7]) {

  int cpt = 0;

  // double lut[83][19][10][8][7];

  for(int i = 0; i < 7; i++) {
    for(int j = 0; j < 8; j++) {
      for(int k = 0; k < 10; k++) {
        for(int l = 0; l < 19; l++) {
          for(int m = 0; m < 83; m++) {
            ptr[m][l][k][j][i] = v[cpt];
            // std::cout << v[cpt] << std::endl;
            cpt = cpt + 1;
          }
        }
      }
    }
  }
}



// [[Rcpp::export]]
std::vector<double> interpol(double thetas, NumericVector lut_1d, double ozone, double taucl, double alb) {

  int nwl = 83;

  double lut_5d[nwl][19][10][8][7];
  to_5d(lut_1d, lut_5d); // Convert lut into a 5d array

  double ed_tmp4[nwl][2][2][2];
  double ed_tmp3[nwl][2][2];
  double ed_tmp2[nwl][2];
  std::vector<double> ed(nwl);

  // Thetas
  std::vector<double> xthetas;
  for(int i = 0; i <= 90; i+=5){
    xthetas.push_back(i);
  }

  // Ozone
  std::vector<double> xozone;
  for(int i = 100; i <= 550; i+=50){
    xozone.push_back(i);
  }

  // Taucl
  std::vector<double> xtaucl;
  xtaucl.push_back(0);
  xtaucl.push_back(1);
  xtaucl.push_back(2);
  xtaucl.push_back(4);
  xtaucl.push_back(8);
  xtaucl.push_back(16);
  xtaucl.push_back(32);
  xtaucl.push_back(64);

  // Albedo
  std::vector<double> xalb;
  for(double i = 0.05; i <= 1; i+=0.15){
    xalb.push_back(i);
  }

  // Find the index where the requested values are.
  int ithetas, iozone, itaucl, ialb;
  double rthetas, rozone, rtaucl, ralb;

  get_indice(xthetas, thetas, ithetas, rthetas);
  get_indice(xozone, ozone, iozone, rozone);
  get_indice(xtaucl, taucl, itaucl, rtaucl);
  get_indice(xalb, alb, ialb, ralb);

  // std::cout << thetas <<  "     " << rthetas <<  "    " << ithetas << std::endl;
  // std::cout << ozone <<  " " << rozone <<  " " << iozone << std::endl;
  // std::cout << taucl <<  " " << rtaucl <<  " " << itaucl << std::endl;
  // std::cout << alb <<  " " << ralb <<  " " << ialb << std::endl;

  // Start interpolation

  int zthetas, zozone, ztaucl;

  // Remove the dimension on Surface Albedo
  for(int i = 0; i <= 1; i++) {

    zthetas = ithetas + i; // Need to fix

    // std::cout << zthetas << std::endl;

    for(int j = 0; j <= 1; j++) {

      zozone = iozone + j; // Need to fix
      // std::cout << zozone << std::endl;

      for(int k = 0; k <= 1; k++) {

        ztaucl = itaucl + k; // Need to fix
        // std::cout << ztaucl << std::endl;

        for(int l = 0; l < nwl; l++) {
          // Line 128
          ed_tmp4[l][i][j][k] = ((1 - ralb) * lut_5d[l][zthetas][zozone][ztaucl][ialb]) + (ralb * lut_5d[l][zthetas][zozone][ztaucl][ialb + 1]);
          // std::cout << ed_tmp4[l][i][j][k] << std::endl;
        }
      }
    }
  }

  // Remove the dimension on taucl
  for(int i = 0; i <= 1; i++) {
    for(int j = 0; j <= 1; j++) {
      for(int l = 0; l < nwl; l++) {
        ed_tmp3[l][i][j] = (1 - rtaucl) * ed_tmp4[l][i][j][0] + rtaucl * ed_tmp4[l][i][j][1];
        // std::cout << ed_tmp3[l][i][j] << std::endl;
      }
    }
  }

  // Remove the dimension on ozone
  for(int i = 0; i <= 1; i++) {
    for(int l = 0; l < nwl; l++) {
      ed_tmp2[l][i] = (1 - rozone) * ed_tmp3[l][i][0] + rozone * ed_tmp3[l][i][1];
      // std::cout << ed_tmp2[l][i] << std::endl;
    }
  }

  // Remove the dimention on sunzenith angle
  for(int l = 0; l < nwl; l++) {
    ed[l] = (1 - rthetas) * ed_tmp2[l][0] + rthetas * ed_tmp2[l][1];
    // std::cout << ed[l] << std::endl;
  }

  return(ed);



}
# /*** R
# # timesTwo(42)
#
# # Sys.setenv("PKG_CXXFLAGS"="-std=c++11")
#
# file <- system.file("extdata", "Ed0moins_LUT_5nm_v2.dat", package = "ed0")
# lut <- scan(file, quiet = TRUE)
# lut <- matrix(lut, ncol = 83, byrow = TRUE)
# lut <- array(lut, dim = c(7, 8, 10, 19, 83))
# lut <- aperm(lut, c(5, 4, 3, 2, 1))
# lut <- as.vector(lut)
#
# latitude <- 69.32548633
# longitude <- -60.96662
# ed_cloud <- interpol(thetas = 86.6804733, lut_1d = lut, ozone = 285.40, taucl = 12.79, alb = 0.2)
# ed_cloud
# ed_clear <- interpol(thetas = 86.6804733, lut_1d = lut, ozone = 285.40, taucl = 0, alb = 0.2)
# ed_clear
#
# cf <- 1
# ed <- ed_cloud * cf + ed_clear * (1 - cf)
# ed
#
#
# modis_file <- "/media/work/r-packages/ed0/inst/extdata/MYD08_D3.A2016280.051.2016281180804.hdf"
# date <- lubridate::parse_date_time(regmatches(modis_file, regexpr("\\d{13}", modis_file)), orders = "YjHMS")
# h <- round(date, units = "day") + lubridate::hours(seq(0, 21, by = 3))
#
# ## Calculate sun position
# thetas <- RAtmosphere::SZA(as.character(h), Lon = longitude, Lat = latitude)
# thetas <- ifelse(thetas >= 90, 89.99, thetas) ## C++ code crashes if thetas = 90.0
# #
# ed_cloud1 <- purrr::map(86.6804733, interpol, lut_1d = lut, ozone = 285.4, taucl =  12.79, alb = 0.2)
#
# # ed_clear <- purrr::map(thetas, interpol, lut_1d = lut, ozone = 285.4, taucl =  0, alb = 0.2)
# #
# # cf <- 1
# #
# # ed_inst <- (ed_cloud[[6]] * cf) + (ed_clear[[6]] * (1 - cf))
# # plot(seq(290, 700, by = 5), ed_inst, type = "l")
#
# #
# # library(tidyverse)
# #
# # df <- data_frame(
# #   wavelength = seq(290, 700, by = 5),
# #   ed0m = ed
# # )
# #
# # df %>%
# #   ggplot(aes(x = wavelength, y = ed0m)) +
# #   geom_point() +
# #   theme_bw()
# #
# # ggsave("/home/pmassicotte/Desktop/test_code_simon.pdf")
# # write_csv(df, "/home/pmassicotte/Desktop/test_code_simon.csv")
#
# # file <- system.file("extdata", "Ed0moins_LUT.dat", package = "ed0")
# # lut <- scan(file, quiet = TRUE)
#
# # test(lut)
#
#
# */
