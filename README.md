R package ed0
================

ed0
---

Installation
------------

The package is currently hosted on GitLab. The latest version can be installed as follow:

``` r
devtools::install_git("https://PMassicotte@gitlab.com/Takuvik/ed0.git")
```

Data structure
--------------

`ed0()` is the main function of the package. It will return a data frame containing:

-   longitude of the pixel
-   latitude of the pixel
-   date\_time (time of the day)
-   cloud\_fraction (0 - 1)
-   cloud\_optical\_tickness (0 - 64)
-   total\_ozone (100 - 550 DU)
-   wavelength (290 - 700 by 5 nm)
-   ed0 (expressed in *μ**E* × *s*<sup>−1</sup> × *m*<sup>−2</sup> × *n**m*<sup>−1</sup>
-   ed\_type (either "ed0m" or "ed0p")

``` r
library(ed0)
library(ggplot2)

modis_file <- system.file("extdata", "MYD08_D3.A2016280.051.2016281180804.hdf", package = "ed0")

latitude <- 69.32548633
longitude <- -60.96662

ed0m <- ed0(modis_file, longitude, latitude, "ed0m")
str(ed0m)
```

    ## Classes 'tbl_df', 'tbl' and 'data.frame':    747 obs. of  9 variables:
    ##  $ longitude             : num  -61 -61 -61 -61 -61 ...
    ##  $ latitude              : num  69.3 69.3 69.3 69.3 69.3 ...
    ##  $ date_time             : POSIXct, format: "2016-10-08 00:00:00" "2016-10-08 00:00:00" ...
    ##  $ cloud_fraction        : num  1 1 1 1 1 ...
    ##  $ cloud_optical_tickness: num  13.7 13.7 13.7 13.7 13.7 ...
    ##  $ total_ozone           : num  291 291 291 291 291 ...
    ##  $ wavelength            : num  290 295 300 305 310 315 320 325 330 335 ...
    ##  $ ed0                   : num  2.00e-07 2.00e-07 3.17e-07 5.01e-07 1.54e-06 ...
    ##  $ ed_type               : chr  "ed0m" "ed0m" "ed0m" "ed0m" ...

Examples
--------

### Compairing ed0+ and ed0-

``` r
library(ed0)
library(ggplot2)

modis_file <- system.file("extdata", "MYD08_D3.A2016280.051.2016281180804.hdf", package = "ed0")

latitude <- 69.32548633
longitude <- -60.96662

ed0m <- ed0(modis_file, longitude, latitude, "ed0m")
ed0m
```

    ## # A tibble: 747 x 9
    ##    longitude latitude  date_time cloud_fraction cloud_optical_tickness
    ##        <dbl>    <dbl>     <dttm>          <dbl>                  <dbl>
    ##  1 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  2 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  3 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  4 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  5 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  6 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  7 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  8 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  9 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## 10 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## # ... with 737 more rows, and 4 more variables: total_ozone <dbl>,
    ## #   wavelength <dbl>, ed0 <dbl>, ed_type <chr>

``` r
ed0p <- ed0(modis_file, longitude, latitude, "ed0p")
ed0p
```

    ## # A tibble: 747 x 9
    ##    longitude latitude  date_time cloud_fraction cloud_optical_tickness
    ##        <dbl>    <dbl>     <dttm>          <dbl>                  <dbl>
    ##  1 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  2 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  3 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  4 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  5 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  6 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  7 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  8 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  9 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## 10 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## # ... with 737 more rows, and 4 more variables: total_ozone <dbl>,
    ## #   wavelength <dbl>, ed0 <dbl>, ed_type <chr>

``` r
df <- rbind(ed0m, ed0p)

ggplot(df, aes(x = wavelength, y = ed0)) +
 geom_line(aes(color = ed_type)) +
 facet_wrap(~date_time) +
 theme_bw()
```

![](README_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-3-1.png)

### Time step

One can use the `time_step` argument to change the temporal resolution used to compute ed0. In the following example, ed0 is requested at every hour.

``` r
library(ed0)
library(ggplot2)

modis_file <- system.file("extdata", "MYD08_D3.A2016280.051.2016281180804.hdf", package = "ed0")

latitude <- 69.32548633
longitude <- -60.96662

ed0m <- ed0(modis_file, longitude, latitude, "ed0m", time_step = 1) ## every 1h
ed0m
```

    ## # A tibble: 2,075 x 9
    ##    longitude latitude  date_time cloud_fraction cloud_optical_tickness
    ##        <dbl>    <dbl>     <dttm>          <dbl>                  <dbl>
    ##  1 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  2 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  3 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  4 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  5 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  6 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  7 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  8 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ##  9 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## 10 -60.96662 69.32549 2016-10-08       0.999668               13.71824
    ## # ... with 2,065 more rows, and 4 more variables: total_ozone <dbl>,
    ## #   wavelength <dbl>, ed0 <dbl>, ed_type <chr>

``` r
ggplot(ed0m, aes(x = wavelength, y = ed0)) +
 geom_line() +
 facet_wrap(~date_time) +
 theme_bw()
```

![](README_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-4-1.png)

Ideas
-----

-   Add `value = c("ed0", "par")` so the user can have either "raw" ed0 or calculated PAR.

Questions:
----------

### Interpolation spatiale des 3 données de MODIS

Dans le fichier `edMODISA.c` il y a la ligne suivante:

``` cpp
ilat = (int)lroundf(89.5 - lat);
```

J'en déduis qu'il n'y a pas d'interpolation spatiale. On va chercher simplement la coordonnée la plus proche en arrondissant la coordonnée demandée par l'utilisateur.

De mon côté j'utilise akima avec les positions brutes, je crois que c'est mieux.
